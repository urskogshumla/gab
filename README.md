## Gabriella

*A game prototype written in C++, using time acceleration 
platforming and problem solving as gameplay features.*

### Contributors  

[Ivan Gabriel Morén](https://github.com/skogshjort): Design, code, concept, 
music.

### About

Please check the file docs/Projektplan for more info on the project.
