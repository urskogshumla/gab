## TODO

### Bugs

* **[ - ]** Fix so that the functions for changing room are private and just change a variable to use in the end of a step.
* **[ check ]** input.X.press and input.X.release doesn't work properly when used within an object class (oJumpSquare)

### Basic Stuff

* **[ - ]** Add mathematical vector functions for setting/getting vector length;
* **[ check ]** Add vectorarrays for passiveSolid and activeSolid objects
* **[ - ]** Add functions for checking collision between a point/RectangleShape and a vectorarray

### Features

* **[ - ]** Add better id:s using enum that are able to
  * **[ - ]** compare to each other using boolean options
  * **[ - ]** translate to enum from a string
* **[ - ]** objectTemplates for active/passive + solid/nonsolid object
