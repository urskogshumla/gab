# Projektplan
*Ivan Morén, Na3B Midgårdsskolan Umeå*

### Varför jag/vi valde detta projekt

With the project I'll have oppurtunity to explore a programming language, but also different art forms like game design, drawing, animating, musical composing. Hopefully the project will inspire other game developers and thinkers.

### Vårt/mitt mål med projektet

Många spelhandlingar idag är inkörda i samma spår, och jag hoppas att projektet skall kunna belysa att egenskaper en vanligtvis kopplar till hjälterollen (mod, viljestyrka etc.) kan finnas oberoende av våld eller konflikt. 

### Problemformulering

Syftet med projektet är att undersöka hur en spelhandling kan utformas så att den inte baserar på konflikt med våld som lösningsmetod.

* Hur kan en skapa en intressant och trovärdig handling som inte baserar på konflikt med våld som lösningsmetod?
* Vad händer om fara uppstår och vad händer om huvudkaraktären dör?
* Går det att avlägsna våld helt från handlingen?

Ur huvudfrågeställningen uppstår fler frågeställningar; går det att avlägsna våld helt från handlingen, vad händer då fara uppstår och vad händer om huvudkaraktären dör, frågor som som inte utgör problem på samma sätt i enkeldimensionella medier som t.ex. filmer eller böcker.

### Metod

In order to investigate the project's questions a game prototype is designed, developed, tested and evaluated. They are then discussed from the process point of view in a paper with research, literature, articles and interviews on the subject in mind. During the process a sketch of possible headers I'd like to include in the paper is formed, so that the amount of work going through my own material is minimized. Sketches and thoughts about the development take form both in text and on physical paper.

I've chosen to work with C++ to implement the prototype as it's a famous kind-of-low level programming language that is used frequently in the industry, but also because I need a substitute for the Windows/OSX program I used when developing games before. C++ is compitable across most operating systems and suits my needs as I currently use Linux based systems only.

### Förstudie

The tools needed to proceed with this project include a computer with C++ standard libraries and a library for game graphics and audio. I'll use my school computer and the library SFML (Simple and Fast MultiMedia Library, an open source basic C++ multimedia library, sfml-dev.org). Since I already have a background in general programming from GML, JS, Python and so on, converting to C++ is a big step but not a huge leap. I have however considered to borrow some books for appendix use in order get comfortable with pointers, memory allocation, low level system design and so on, and since C++ is such a big language finding such literature shouldn't be a problem.

### Avgränsning

Since I don't have enough time to write a thesis on the whole subject of game design and conflict violence I have narrowed the project around the process of creating a game prototype that meets the conditions and the problems and desicions that comes with it. My ultimate goal is naturally to finish the whole game, but a playable room where one can push some objects around and manipulate time acceleration should illustrate the concept enough to give it to a test group. The primary focus while developing the game is the relation between physics and gameplay, but eventually also aesthetics and communicative design.
