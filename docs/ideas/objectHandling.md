## **Objects**

### Differentiating Objects

    objects.solid.at(temp) -> blowUp();

All objects are stored using pointers in vectors. Every object currently is existing must have a pointer in objects.all, so that the whole system can be cleaned from memory leaks through a simple function.

Vectors for the different basic types of objects are more free to change and simply aim at lowering the looptime through object lists. All objects that needs to be checked for collision exists for example under objects.solid, and all objects that are potentially drawn in the window are to be found in objects.visual.

### Deletion

    virtual void baseObject::delete() {  
      if (!permanent)  
        delete this;  
    };

Deletion of objects doesn't actually happen from the view class. Maybe via an override, to use when SIGINTERRUPT is called or something, but mostly from the own object class. By doing this, each object can then decide whether to just delete itself or if it wants to remove itself from the collisions and continue to play a sound or whatever.

### Handling object types

    if (temp == objects.types.explosionArea)
      temp -> blowUp();

Since there will be a lot of different objects, it must be easy to check if an object from a vector or a collision is a specific type of object. All different functions and variables of the different objects can't exist in every object, that would blow up the RAM, instead the pointers must be casted before making attempts of calling object specific parts. I'll write more when I've researched this further.
