## Components and their Bridges

So, this is my latest idea, based on the *Component Pattern* commonly used to slice things up with. Basically all objects will consist of one or more *Components*, in this case a combination of components defining whether and how the object is active, visible, and physical.

Most activity will happen in these components, but the ones binding object parts together will be the *Component Bridges*. These bridges will consist of shared data and broadcast messages between their different components.

### Just a draft

And I really mean it. Static physical objects isn't really enough to describe platforms, popping leaves and other stuff that move or change with time but do not receive impact from other physical objects. And CalcObjects should be a bare minimum, without STATE, position or size. So please do consider this a WIP.

### Component Bridge Types and their shared data:

    all types => STATE, size, position

    active => update(); { Private Data }

    physical => collisions, layer, solid { Friend Data }

    visual => draw(); { Private Data }

    dynamic => velocity, list of forces

Pretty straightforward. If an object wants to update every step, then its component bridge will be a child of the *active* Component Bridge Type, and if it wants physics stuff it inherits the shared data of the *physical* Component Bridge Type. To move around it will have to *dynamic*, and in order to get drawn it must be *visual* too.



### Component Bridges

Okay, here goes some basic types that I've come up with:

    CalcObject - active object.


    PhysicalObject - active, solid-physical, visible and dynamic object.

    PhysicalGround - solid-physical, visible object.

    PhysicalArea - active and nonsolid-physical object.


    BackgroundObject - visual object.

    ActiveBackgroundObject - active and visual object.

CalcObjects are objects that just hold data and calculates stuff, like controller objects.

Physical stuff refers to objects that interact physically. P-Objects are dynamic, moves around and knocks each other, P-Grounds are static, like ground or walls, P-Areas are bounding boxes that can take action on collision, like sound emitters or cut scene activators.

Background objects are just visual objects, active or passive. Physical background objects could be created using a custom layer so that they only interact with certain objects, the static ones for example.
