## Markup for rooms

*Contains objects with object index, position, z, customStartCode.*

### Markup ideas

    1. id1 x y z
    2. id1 x y z
    3. id2 x y z

>*Current implementation*

    1. id1 x y z customStartCode
    2. id1 x y z customStartCode
    3. id2 x y z customStartCode
    
>*Not sorted at all. Can be used to control drawing order by adding objects to file in the order I generate them.*

    1. id1: x1 y1 z1 customStartCode1 [...] xN yN zN customStartCodeN
    2. id2: x1 y1 z1 customStartCode1 [...] xN yN zN customStartCodeN
    3. id3: x1 y1 z1 customStartCode1 [...] xN yN zN customStartCodeN

>*Sorted by id:s. Easier to manage manually since it's crisp clear where every object of a type is.*

### TODO

* [CHECK!] Read from file.gab and generate objects out of positions and indexes.
* [ TODO ] Think about possible ways to implement customStartCode (without having the room file in C++ that is)
* [ TODO ] Implement an id-system, maybe with a classList as a string and a function for checking, adding, removing and toggling classes?
