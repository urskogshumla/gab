## Collision Solving

*Making objects' physical data communicate in order to avoid ghosts.*

A challenge have been solving collisions. Since the time steps in *Gabriella* are dynamically changing, the same code must be able to handle collisions over both short and very long distances. Furthermore, collision solving must be *fairly* consistent and independent of time velocity. Rounding errors due to floats are a flaw of Mother Nature, and thus respectfully accepted (read: ignored).


### Box of the collision solving kitten

The limiting framework of a collision solver consists of the types of dimensions and shapes it must handle. In this prototype two-dimensional rectangles interact on layers in a single dimension of time to keep it simple and creative.

### The lonely axes


As an early concept, the two axes of movement were calculated separately. This works pretty well, until an object realize it's biggest dream is to move diagonally. When it does, *jumping* may occur. When talking about jumping on a physics topic, the term most commonly refers to unpleasant event of two objects interfering in the middle of a jump but not noticing each other's presence. The smaller steps that are calculated, the less jumping. Since the steps in *Gabriella* i theory may be infinitely large or small, the method's result differ very much with time velocity.

### Fronts and backs back in business on the front line

Approaching corner against back sides, two different cases.

Equation for linear functions where the corner's normal meet the other corner in order to find out which corner to use. Super complex.

![](../images/boxes.png)

### Algebra and vectors

Setting x/y diffs as functions of time in percentage of a time step. Simple vector calculation <nobr>k = (B - A) ÷ (v - u)</nobr>.

![](../images/collisionSketch_masked.png)
<i>Linear equations explaining how k is calculated so that (A + k.c \* v).c == (B + k.c \* v).c where c is the axes where the objects meet.</i>

### Boundaries of bounding boxes

Since solving a collision is so effective, it can complement the bounding box interference function. Checking polygons can therefore be kind of rusty and clumsy. In the prototype the two outer corners, the end position front corner and the start position back corner, define a rectangle that is used when checking potential interference with other moved boxes.

![](../images/boundingBoundingBoxHalves.png)![](../images/boundingBoundingBoxQuarters.png)
