## Fysikmotorns råddiga tillvaro

Eftersom tiden som förflutit för varje step i fysikmotorn varierar har jag nu senast tänkt att den skall vara oberoende av tidshastighet. Den är då inte bunden till steps på något vis, varken i eller utanför mekaniken. Då jag behandlar båda tidsriktningar precis likadant är det stora kneket hur samma resultat skall fås oavsett om små eller stora mängder världstid förflyter.

### Implementation

Hänsyn måste tas till hur hastigheterna hos objekten förändrats under den tid som förflutit. Krasst måste vi ta det i mindre delar, så få som möjligt. Vi räknar därför varje hastighet för sig fram tills nästa hastighetsbyte hos något objekt, och estimerar var kollisioner borde inträffa genom att först räkna ut en yttre kollisionsrektangel för varje objekt som inrymmer dess alla positioner för tidsperioden.

### Workflow

- Räkna ut yttre kollisionsrektanglar för den förflutna tiden.
- Räkna alla potentiella kollisioner och notera när de som sker sker.
- Utför den tidigaste outförda kollisionen AB.
- Räkna ut yttre kollisionrektanglar för A och B (eller alla) från tidspunkten AB.
- Räkna alla potentiella kollisioner för A och B från tidspunkten AB och notera när de som sker sker.
- Upprepa från punkt tre tills inga fler outförda kollisioner finns.

### Noteringar

Ju mindre tidshastighet, desto färre kollisioner per step. Frameraten har (nästan) direkt koppling till hur mycket som räknas ut mellan varje frame, så generellt borde spelets framerate vara högre då det går i slowmotion. Men eftersom skärmens kapacitet begränsar kommer skillnaden inte alltid finnas till.

Acceleration hos ett objekt kan sparas som en länkad lista med tidpunkter och acceleration, eller räknas ut om det istället är krafter som sparas så att skillnad kan göras på t.ex. friktion och gravitation (varför en nu skulle vilja det). Då värdet x för en tidpunkt t i listan L skall ges söks L från x.index = L/ 2 en fjärdedel åt rätt håll, sedan 1/ 8, 1/ 16 och så vidare tills x.t <= t och (x.index + 1).t är > t eller vice versa.

Dock borde det finnas ett minimistep för kraftförändring, t.ex. 100 Hz eller så. Annars kan det bli supermånga lagrade krafter som bara utförs jättekort eftersom k inte kommer vara kvantifierat.

Jag noterar också att jag jobbar förvånansvärt ofta vid klockan tre, fyra på morgonen. Sjukt ovärt.
