#include "collisionRectangleShape.hpp"

using namespace sf;

bool getCollision(RectangleShape a, std::vector<baseObject*> n) {
	for (int i = 0; i < (int)n.size(); i++)
		if (getCollision(a, n.at(i)->box)) return true;
	return false;
}
bool getCollision(Vector2f a, std::vector<baseObject*> n) {
	for (int i = 0; i < (int)n.size(); i++)
		if (pointWithinRect(a, n.at(i)->box)) return true;
	return false;
}

bool rectOverlappingRect(RectangleShape a, RectangleShape b) {
	if (a.getPosition().x >= b.getPosition().x
		&& a.getPosition().x + a.getSize().x - 1 <= b.getPosition().x + b.getSize().x - 1
		&&  a.getPosition().y <= b.getPosition().y
		&& a.getPosition().y + a.getSize().y - 1 >= b.getPosition().y + b.getSize().y - 1) {
			return true;
		}
		return false;
}

bool pointWithinRect(Vector2f a, RectangleShape b) {
	return a.x >= b.getPosition().x
		&&  a.y >= b.getPosition().y
		&&  a.x <= b.getPosition().x + b.getSize().x - 1
		&&  a.y <= b.getPosition().y + b.getSize().y - 1;
}

bool rectWithinRect(RectangleShape a, RectangleShape b) {
	if (pointWithinRect(a.getPosition(), b)) return true;
	if (pointWithinRect(a.getPosition() + Vector2f(a.getSize().x - 1, 0), b)) return true;
	if (pointWithinRect(a.getPosition() + Vector2f(0, a.getSize().y - 1), b)) return true;
	if (pointWithinRect(a.getPosition() + a.getSize() - Vector2f(1,1), b)) return true;
	return false;
}

bool getCollision(RectangleShape a, RectangleShape b) {
	//Trimming negative sizes
	if (a.getSize().x < 0) {
		a.setPosition(Vector2f(a.getPosition().x + a.getSize().x, a.getPosition().y));
		a.setSize(Vector2f(-a.getSize().x, a.getSize().y));
	}
	if (a.getSize().y < 0) {
		a.setPosition(Vector2f(a.getPosition().x, a.getPosition().y + a.getSize().y));
		a.setSize(Vector2f(a.getSize().x, -a.getSize().y));
	}
	if (b.getSize().x < 0) {
		b.setPosition(Vector2f(b.getPosition().x + b.getSize().x, b.getPosition().y));
		b.setSize(Vector2f(-b.getSize().x, b.getSize().y));
	}
	if (b.getSize().y < 0) {
		b.setPosition(Vector2f(b.getPosition().x, b.getPosition().y + b.getSize().y));
		b.setSize(Vector2f(b.getSize().x, -b.getSize().y));
	}

	if (a.getPosition().x >= b.getPosition().x + b.getSize().x) return false;
	if (a.getPosition().x + a.getSize().x <= b.getPosition().x) return false;
	if (a.getPosition().y >= b.getPosition().y + b.getSize().y) return false;
	if (a.getPosition().y + a.getSize().y <= b.getPosition().y) return false;
	return true;

	if (rectWithinRect(a, b)) 		return true;
	if (rectWithinRect(b, a)) 		return true;
	if (rectOverlappingRect(a, b)) 	return true;
	if (rectOverlappingRect(b, a)) 	return true;

	return false;
}
