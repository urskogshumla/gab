#ifndef __COLLISIONRECTANGLESHAPE_INCLUDED__
#define __COLLISIONRECTANGLESHAPE_INCLUDED__

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <vector>
#include "../objects/objects.hpp"

bool getCollision(sf::RectangleShape, std::vector<baseObject*>);
bool getCollision(sf::Vector2f, std::vector<baseObject*>);
bool getCollision(sf::RectangleShape, sf::RectangleShape);
bool rectOverlappingRect(sf::RectangleShape, sf::RectangleShape);
bool pointWithinRect(sf::Vector2f, sf::RectangleShape);
bool rectWithinRect(sf::RectangleShape, sf::RectangleShape);

#endif
