#include "collisionRectangleShape.hpp"

using namespace sf;

bool getCollision(RectangleShape a, std::vector<baseObject*> n) {
	for (int i = 0; i < (int)n.size(); i++)
		if (getCollision(a, n.at(i)->box)) return true;
	return false;
}
bool getCollision(Vector2f a, std::vector<baseObject*> n) {
	for (int i = 0; i < (int)n.size(); i++)
		if (getCollision(a, n.at(i)->box)) return true;
	return false;
}

bool getCollision(Vector2f a, RectangleShape b) {
	return a.x >= b.getPosition().x
		&&  a.y >= b.getPosition().y
		&&  a.x <= b.getPosition().x + b.getSize().x - 1
		&&  a.y <= b.getPosition().y + b.getSize().y - 1;
}

bool getCollision(RectangleShape a, RectangleShape b) {
	return !(a.getPosition().x >= b.getPosition().x + b.getSize().x
	||  a.getPosition().x + a.getSize().x <= b.getPosition().x
	||  a.getPosition().y >= b.getPosition().y + b.getSize().y
	||  a.getPosition().y + a.getSize().y <= b.getPosition().y);
}




//If sizes are negative:
/*
RectangleShape a trimRectangleShape(RectangleShape a) {
	if (a.getSize().x < 0) {
		a.setPosition(Vector2f(a.getPosition().x + a.getSize().x, a.getPosition().y));
		a.setSize(Vector2f(-a.getSize().x, a.getSize().y));
	}
	if (a.getSize().y < 0) {
		a.setPosition(Vector2f(a.getPosition().x, a.getPosition().y + a.getSize().y));
		a.setSize(Vector2f(a.getSize().x, -a.getSize().y));
	}
	return a;
}
*/
