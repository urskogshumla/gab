#ifndef __GABRIELLA_FUNCTIONS_COLLISION_RECTANGLESHAPE__
#define __GABRIELLA_FUNCTIONS_COLLISION_RECTANGLESHAPE__

#include <vector>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "../objects/objects.hpp"

bool getCollision(sf::RectangleShape, std::vector<baseObject*>);
bool getCollision(sf::Vector2f, std::vector<baseObject*>);
bool getCollision(sf::RectangleShape, sf::RectangleShape);
bool getCollision(sf::Vector2f, sf::RectangleShape);
//RectangleShape trimRectangleShape(RectangleShape);

#endif
