#include "console.hpp"

#include <iostream>

void consoleHandler::header(string pString, string endString) {
  cout << endl << DEBUG_INDENT << "[" << pString << "] " << endString;
  return;
};

void consoleHandler::text(string pString, string endString) {
  cout << DEBUG_INDENT << DEBUG_INDENT << pString << " " << endString;
  return;
};

void consoleHandler::var(string paramVar, string paramValue, string endString) {
  cout << DEBUG_INDENT << DEBUG_INDENT << paramVar << ": ";
  for (unsigned int i = paramVar.length(); i < 12; i++)
    cout << " ";
  cout << paramValue << endString;
  return;
};

string consoleHandler::getInput(string variable) {
  text("Please fill in " + variable, ": ");
  string temp = "";
  getline(cin, temp);
  return temp;
};

consoleHandler debug;
