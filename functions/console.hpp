#ifndef __GABRIELLA_FUNCTIONS_DEBUG__
#define __GABRIELLA_FUNCTIONS_DEBUG__

#include <string>

#define DEBUG_INDENT "   "

using namespace std;

class consoleHandler {
public:
  void header(string, string);
  void text(string, string);
  void var(string, string, string);

  string getInput(string);

  //Default endString functions
  void header(string paramString) { header(paramString, "\n"); };
  void text(string paramString) { text(paramString, "\n"); };
  void var(string paramVar, string paramValue) { var(paramVar, paramValue, "\n"); };
};

extern consoleHandler debug;

#endif
