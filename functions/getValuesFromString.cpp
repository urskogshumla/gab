#include "getValuesFromString.hpp"
#include <regex>

using namespace std;

regex REGEX_FLOATS("[-]?(([0-9]+(\\.[0-9]+)?)|(\\.[0-9]+))"); //Not using scientific 32*e(z) e(x)=10^x

vector<float> getValuesFromString(string str) {
  vector<float> values;
  float value;
  smatch regex_matches;

  while (true) {
    if (!regex_search(str, regex_matches, REGEX_FLOATS)) break;
    while (!regex_search(str, regex_matches, REGEX_FLOATS, regex_constants::match_continuous)) {
      str.erase(0, 1);
    }
    sscanf(regex_matches[0].str().c_str(), "%f", &value);
    values.push_back(value);
    str.erase(0, regex_matches[0].str().length());
  }
  return values;
}

//Returns float values found in string str using the regex REGEX_FLOATS
//FIXME: Check position of first match and erase to that position. (Line 20)
