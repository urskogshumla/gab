#ifndef __GABRIELLA_FUNCTIONS_GETVALUESFROMSTRING__
#define __GABRIELLA_FUNCTIONS_GETVALUESFROMSTRING__

#include <vector>
#include <string>

std::vector<float> getValuesFromString(std::string);

#endif
