/*
	Gabriella base code is licenced Creative Commons.
	All the graphics are copyrighted
	All ideas are copyrighted.
	Everything is open source.
	I am the source.

	Best regards and (c) 2014,
	Ivan Morén
*/

/*
	Globals:
		"app":		Handles view and input.
		"view":		Handles objects, drawing and viewport.
		"input":	Handles keyboard input.
		"objects":	Vector for all current objects
*/

#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <string>

#include "system/window.hpp"
#include "system/input.hpp"
#include "system/view.hpp"
#include "system/app.hpp"
#include "functions/console.hpp"

#include "functions/getValuesFromString.hpp"

using namespace sf;

int main() {

	app.initWindow();
	app.displayInfo();

	view.loadRooms();
	view.setRoom(0);

	while (window.isOpen()) {
		Event windowAction;
		while (window.pollEvent(windowAction)) {
			if (windowAction.type == Event::Closed)
				window.close();
		}

		if (input.ESCAPE.press)
			window.close();

		/*if (input.ALFA.press)
			view.changeRoom(1);
		else if (input.BETA.press)
			view.changeRoom(-1);
*/
		/*if (input.DOWN.press and input.LEFT.active and input.RIGHT.active) {
			vector<float> temp = getValuesFromString(debug.getInput("id, x, y and z values."));
			view.addObject(temp, true);
		}*/


		app.update();
	}

	view.cleanView();

	return 0;
}
