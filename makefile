CC			= g++

#DEBUG		= -g
CFLAGS		= -Wall -std=c++11 $(DEBUG)
LFLAGS		= -Wall -std=c++11 $(DEBUG)

TARGET		= Program

SRCS		= \
main.cpp $(wildcard system/*.cpp objects/*.cpp functions/*.cpp)
#$(addprefix system/, RobotData.cpp)

OBJS		= $(SRCS:.cpp=.o)

#DEFINES		=

#INCLUDES	=

#LIBS		=

LINKS		= -lsfml-window -lsfml-audio -lsfml-graphics -lsfml-system

build: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(LFLAGS) -o $(TARGET) $(OBJS) $(LIBS) $(LINKS)

.cpp.o: $<
	$(CC) $(CFLAGS) -c $< -o $@ $(INCLUDES) $(DEFINES)

clean:
	rm -fv $(OBJS) $(TARGET)

rebuild: clean build

valgrind: $(TARGET)
	(valgrind --show-reachable=yes --leak-check=full -v ./$(TARGET))

run:
	./$(TARGET)

depend: $(SRCS)
	makedepend -m -Y $^ $(INCLUDES)

# DO NOT DELETE THIS LINE -- make depend needs it

main.o: system/window.hpp system/input.hpp system/view.hpp
main.o: objects/objects.hpp system/app.hpp system/input.hpp system/view.hpp
main.o: system/app.hpp functions/console.hpp
main.o: functions/getValuesFromString.hpp
system/input.o: system/input.hpp system/app.hpp
system/view.o: system/view.hpp objects/objects.hpp system/app.hpp
system/view.o: system/input.hpp system/view.hpp objects/testObjects.hpp
system/view.o: objects/objects.hpp objects/kollisionsTest.hpp
system/view.o: system/window.hpp functions/getValuesFromString.hpp
system/view.o: functions/console.hpp
system/window.o: system/window.hpp system/app.hpp
system/app.o: system/app.hpp system/window.hpp system/input.hpp
system/app.o: system/view.hpp objects/objects.hpp system/app.hpp
system/app.o: system/input.hpp system/view.hpp functions/console.hpp
objects/testObjects.o: objects/testObjects.hpp objects/objects.hpp
objects/testObjects.o: functions/collisionRectangleShape.hpp
objects/testObjects.o: objects/objects.hpp system/app.hpp system/input.hpp
objects/testObjects.o: system/view.hpp functions/console.hpp
objects/testObjects.o: system/window.hpp
objects/kollisionsTest.o: objects/kollisionsTest.hpp objects/objects.hpp
objects/kollisionsTest.o: system/window.hpp functions/console.hpp
objects/objects.o: objects/objects.hpp system/window.hpp
functions/console.o: functions/console.hpp
functions/getValuesFromString.o: functions/getValuesFromString.hpp
functions/collisionRectangleShape.o: functions/collisionRectangleShape.hpp
functions/collisionRectangleShape.o: objects/objects.hpp system/app.hpp
functions/collisionRectangleShape.o: system/input.hpp system/view.hpp
