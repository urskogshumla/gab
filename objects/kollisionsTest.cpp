#include "kollisionsTest.hpp"
#include <math.h>

using namespace sf;

Vector2f vMultiply(Vector2f a, Vector2f b) {
	return Vector2f(a.x * b.x, a.y * b.y);
};
Vector2f vDivide(Vector2f a, Vector2f b) {
	//if (b.x == 0) { /*b.x = 0.00000001;*/ debug.text("JOY DIVISION BY ZERO!" + to_string(b.y)); }
	//if (b.y == 0) { /*b.y = 0.00000001;*/ debug.text("JOY DIVISION BY ZERO!" + to_string(b.x)); }
	return Vector2f(a.x / b.x, a.y / b.y);
};
Vector2f vAbs(Vector2f a) {
	return Vector2f((a.x < 0)?-a.x:a.x, (a.y < 0)?-a.y:a.y);
}
Vector2f vPosNeg(Vector2f a) {
	return Vector2f((a.x > 0)?1:((a.x < 0)?-1:0), (a.y > 0)?1:((a.y < 0)?-1:0));
}

kolTest::kolTest() {
	base = new kvadrat(70, 70, Color(255, 50, 50));
	base->pos = Vector2f(10,10);
	target = new kvadrat(130, 40, Color(50, 255, 50));
	target->pos = Vector2f(100, 250);
	sBG = RectangleShape(Vector2f(640.f, 480.f));
	sBG.setPosition(Vector2f(0,0));
	sBG.setFillColor(Color::Black);
	active = base;
	passive = target;
}

void kolTest::update() {
	if (input.BETA.press) toggleActive();
	active->update(active, passive);
	active->updateCollision(active, passive);
	passive->updateCollision(active, passive);
}

void kolTest::toggleActive() {
	if (active == base) active = target;
	else active = base;
	if (active == base) passive = target;
	else passive = base;
}

void kolTest::draw() {
	window.draw(sBG);
	base->draw(true);
	target->draw(true);
}


kolTest::~kolTest() {
	delete base;
	delete target;
}

float getCollisionPoint(kvadrat *a, kvadrat *b) {
	if (a->size.x * a->size.y == 0 or b->size.x * b->size.y == 0) return -1;
	Vector2f A = a->corner(a->vel);
	Vector2f B = b->corner(-(a->vel));
	Vector2f result = vDivide(B - A, a->vel - b->vel);
	bool xWithinBounds = result.x > 0 && result.x < 1;
	bool yWithinBounds = result.y > 0 && result.y < 1;
	if (xWithinBounds) {
		A = a->pos + a->vel * result.x;
		B = b->pos + b->vel * result.x;
		if (A.x > B.x + (b->size.x + 1) || A.x <= B.x - (a->size.x + 1) || A.y > B.y + (b->size.y + 1) || A.y <= B.y - (a->size.y + 1))
			xWithinBounds = false;
	}
	if (yWithinBounds) {
		A = a->pos + a->vel * result.y;
		B = b->pos + b->vel * result.y;
		if (A.x > B.x + (b->size.x + 1) || A.x <= B.x - (a->size.x + 1) || A.y > B.y + (b->size.y + 1) || A.y <= B.y - (a->size.y + 1))
			yWithinBounds = false;
	}

	if (xWithinBounds && yWithinBounds) return (result.x < result.y)?result.x:result.y;
	if (xWithinBounds) return result.x;
	if (yWithinBounds) return result.y;
	return -1;
}

void kvadrat::update(kvadrat *me, kvadrat *k) {
	modCurrent = &pos;
	if (input.ALFA.press and input.LEFT.passive and input.RIGHT.passive and input.UP.passive and input.DOWN.passive) {
		if (modSecondary == &size) modSecondary = &vel;
		else modSecondary = &size;
	}
	else {
		if (input.ALFA.active) modCurrent = modSecondary;

		if (input.LEFT.active) modCurrent->x -= 1;
		else if (input.RIGHT.active) modCurrent->x += 1;
		if (input.UP.active) modCurrent->y -= 1;
		else if (input.DOWN.active) modCurrent->y += 1;
	}
	//Move collisionsquare to right position
}

void kvadrat::updateCollision(kvadrat *me, kvadrat *k) {
	zx = getCollisionPoint(me, k);
}
