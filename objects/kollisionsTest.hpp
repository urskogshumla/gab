#ifndef __GABRIELLA_OBJECT_KOLLISIONSTEST__
#define __GABRIELLA_OBJECT_KOLLISIONSTEST__

#include "objects.hpp"
#include <SFML/Graphics/RectangleShape.hpp>
#include "../system/window.hpp"
#include "../functions/console.hpp"

enum Corner { TR, TL, BR, BL, RT, LT, RB, LB };

Vector2f vMultiply(Vector2f,Vector2f);
Vector2f vDivide(Vector2f, Vector2f);
Vector2f vAbs(Vector2f);

class kvadrat {
private:
	RectangleShape _shape;
	Vector2f *modSecondary = NULL, *modCurrent = NULL;
	float zx, zy;
public:
	Vector2f size, pos, vel;
	Color color;
	bool active;
	RectangleShape getShape(Vector2f pos_) {
		_shape.setPosition(pos_);
		_shape.setSize(size);
		_shape.setFillColor(color);
		return _shape;
		_shape.setOutlineColor(color);
		return _shape;
	}
	kvadrat(float sizeX_, float sizeY_, Color color_) {
		size = Vector2f(sizeX_, sizeY_);
		_shape = RectangleShape(size);
		_shape.setOutlineThickness(0);
		_shape.setFillColor(Color(0,0,0,0));
		color = color_;
		pos = Vector2f(0,0);
		vel = Vector2f(0,0);
		modSecondary = &size;
	}

	void update(kvadrat *, kvadrat *);
	void updateCollision(kvadrat *, kvadrat *);

	Vector2f corner(Vector2f v_) {
		return pos + (size * 0.5f) + vMultiply(size * 0.5f, vDivide(v_, vAbs(v_)));
	}
	void draw(bool dCol) {
		color.a = 255;
		getShape(pos + vel);
		window.draw(_shape);
		color.a = 50;
		getShape(pos);
		window.draw(_shape);
		if (!dCol || zx < 0 || zx > 1) return;
		color.a = 140;
		getShape(pos + (vel * zx));
		window.draw(_shape);
		_shape.setSize(Vector2f(2,2));
		_shape.setPosition(vel + corner(vel) - (_shape.getSize() / 2.0f));
		color.a = 255;
		window.draw(_shape);
	}
};

class kolTest : public baseObject {
private:
	kvadrat *base, *target, *active, *passive;
	void toggleActive();
	RectangleShape sBG;
public:
	kolTest();
	void update();
	void draw();
	~kolTest();
};

#endif
