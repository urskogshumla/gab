#include "objects.hpp"
#include "../system/window.hpp"

baseObject::baseObject() {
	persistent = false;
	RectangleShape box(Vector2f(0,0));
	pos = Vector2f(0,0);
}

void baseObject::update() {};
void baseObject::refresh() {
	currentImage.setPosition(pos);
};
void baseObject::draw() {
	window.draw(currentImage);
};

objectList objects;
