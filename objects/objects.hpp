#ifndef __GABRIELLA_OBJECT__
#define __GABRIELLA_OBJECT__

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

//These are in header file so that every object.hpp file includes it
#include "../system/app.hpp"
#include "../system/input.hpp"
#include "../system/view.hpp"

using namespace sf;
using namespace std;

//TODO: Typer av objekt:
//
//	* BASEOBJECT: bool persistent, update, refresh, draw
//	* PASSIVT NONSOLID: draw()
//	* AKTIVT NONSOLID: update()

class baseObject {
	public:
		bool persistent;
		int z;
		RectangleShape box;
		Vector2f pos;
		Sprite currentImage;
		baseObject();
		virtual void update();
		virtual void refresh();
		virtual void draw();
		virtual ~baseObject() {};
};

class objectList {
	public:
		vector<baseObject*> all;
		vector<baseObject*> solid;
		vector<baseObject*> nonsolid;
		vector<baseObject*> active;
		vector<baseObject*> passive;

		objectList() {};
		~objectList() {};
};

extern objectList objects;

#endif
