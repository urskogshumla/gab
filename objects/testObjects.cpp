#include "testObjects.hpp"
#include "../functions/collisionRectangleShape.hpp"
#include "../functions/console.hpp"
#include "../system/window.hpp"
#include <math.h>

Vector2f GRAVITY(0,1);

audioHandler::audioHandler() {
	checkPoint = 0;
	_snJump1.loadFromFile("audio/jump1.wav");
	_snJump2.loadFromFile("audio/jump2.wav");
	_snFinishLevel.loadFromFile("audio/finishLevel.wav");
	_snBelowScreen.loadFromFile("audio/belowScreen.wav");
	_snCheckPoint.loadFromFile("audio/checkpoint.wav");
	snJump1.setBuffer(_snJump1);
	snJump2.setBuffer(_snJump2);
	snFinishLevel.setBuffer(_snFinishLevel);
	snBelowScreen.setBuffer(_snBelowScreen);
	snCheckPoint.setBuffer(_snCheckPoint);
	snCheckPoint.setVolume(200);
}

audioHandler audio;

oJumpGround::oJumpGround() {
	box = RectangleShape(Vector2f(80,30));
}

oJumpGoal::oJumpGoal() {
	box = RectangleShape(Vector2f(30,30));
	box.setFillColor(Color(255,120,0));
}

oJumpCheckPoint::oJumpCheckPoint() {
	box = RectangleShape(Vector2f(30,30));
	box.setFillColor(Color::Yellow);
	alreadyTaken = audio.checkPoint >= view.getCurrentRoomIndex();
	taken = false;
}

oJumpGravity::oJumpGravity() {
	box = RectangleShape(Vector2f(30,30));
	box.setFillColor(Color::Red);
	taken = false;
}

oJumpGravity::~oJumpGravity() {
	GRAVITY.y = 1;
}

void oJumpCheckPoint::update() {
	box.setPosition(pos);
	if (getCollision(box, objects.active) && !taken) {
		if (!alreadyTaken) {
			audio.checkPoint = view.getCurrentRoomIndex();
			debug.header("Reached the checkpoint for room " + to_string(view.getCurrentRoomIndex()));
		}
		audio.snCheckPoint.play();
		taken = true;
	}
}

void oJumpGravity::update() {
	box.setPosition(pos);
	if (getCollision(box, objects.active) && !taken) {
		audio.snCheckPoint.play();
		taken = true;
		GRAVITY = -GRAVITY;
	}
}

oJumpSquare::oJumpSquare() {
	jumpAgain = true;
	box = RectangleShape(Vector2f(30,30));
	box.setFillColor(Color::White);
	F = Vector2f();
	v = Vector2f();
}

float oJumpSquare::cut(float x, float y, float z) {
	if (x > z) return z;
	if (x < y) return y;
	return x;
}

void oJumpSquare::update() {
	F = GRAVITY;
	pos.y += GRAVITY.y;
	box.setPosition(pos);
	if (input.LEFT.active && getCollision(box, objects.solid))
		F.x = -.2;
	if (input.RIGHT.active && getCollision(box, objects.solid)) {
		F.x = .2;
	}
	if (input.RIGHT.passive and input.LEFT.passive and getCollision(box, objects.solid)) {
		if (v.x < 0)
			F.x = cut(.4, 0, -v.x);
		else if (v.x > 0)
			F.x = cut(-.4, -v.x, 0);
		else
			F.x = 0;
	}

	if (input.UP.press && (getCollision(box, objects.solid))) {
		F.y = -12 * std::abs(v.x) * GRAVITY.y;
		jumpAgain = true;
		audio.snJump1.setVolume(std::abs(v.x) / 3 * 40 + 20);
		audio.snJump1.play();
	}
	else if (input.UP.press && jumpAgain) {
		F.y = (-12 * std::abs(v.x)) * GRAVITY.y;
		jumpAgain = false;
		if (input.LEFT.active)
			v.x = -std::abs(v.x);
		if (input.RIGHT.active) {
			v.x = std::abs(v.x);;
		}
		audio.snJump2.setVolume(std::abs(v.x) / 3 * 40 + 20);
		audio.snJump2.play();
	}
	pos.y -= GRAVITY.y;
	box.setPosition(pos);
	v += F;
	if (v.y > 17.5 + GRAVITY.y * 2.5) v.y = 17.5 + GRAVITY.y * 2.5;
	if (v.y < -17.5 + GRAVITY.y * 2.5) v.y = -17.5 + GRAVITY.y * 2.5;
	if (v.x > 3) v.x = 3;
	if (v.x < -3) v.x = -3;
	pos.y += v.y;
	box.setPosition(pos);
	if (getCollision(box, objects.solid)) {
		pos.y -= v.y;
		v.y = v.y / std::abs(v.y) * 0.1;
		box.setPosition(pos);
		while (!getCollision(box, objects.solid)) { pos.y += v.y; box.setPosition(pos); }
		pos.y -= v.y;
		v.y = 0;
	}
	pos.x += v.x;
	box.setPosition(pos);
	if (getCollision(box, objects.solid)) {
		pos.x -= v.x;
		v.x = v.x / std::abs(v.x);
		box.setPosition(pos);
		while (!getCollision(box, objects.solid)) { pos.x += v.x; box.setPosition(pos); }
		pos.x -= v.x;
		v.x = 0;
	}
	if (getCollision(box, objects.passive)) { //pos + Vector2f(box.getSize().x / 2, box.getSize().y / 2) for middle
		if (!view.changeRoom(1)) view.setRoom(0);
		audio.snFinishLevel.play();
	}
	if (pos.y * GRAVITY.y > window.getSize().y / 2 + window.getSize().y / 2 * GRAVITY.y) {
		view.setRoom(audio.checkPoint);
		audio.snBelowScreen.play();
	}
}

void oJumpSquare::draw() {
	box.setPosition(pos);
	window.draw(box);
}

void oJumpGround::draw() {
	box.setPosition(pos);
	window.draw(box);
}

void oJumpGoal::draw() {
	box.setPosition(pos);
	window.draw(box);
}

void oJumpCheckPoint::draw() {
	if (taken and box.getSize().y < 1024) {
		pos.x -= box.getSize().x / 8;
		pos.y -= box.getSize().y / 8;
		box.setSize(Vector2f(box.getSize().x / 4 * 5, box.getSize().y / 4 * 5));
	}
	box.setPosition(pos);
	window.draw(box);
}

void oJumpGravity::draw() {
	if (taken and box.getSize().y < 1024) {
		pos.x -= box.getSize().x / 8;
		pos.y -= box.getSize().y / 8;
		box.setSize(Vector2f(box.getSize().x / 4 * 5, box.getSize().y / 4 * 5));
	}
	box.setPosition(pos);
	window.draw(box);
}

oSquare::oSquare() {
	boxi = RectangleShape(Vector2f(30,30));
	window.close();
}

oCircle::oCircle(float pX, float pY, float pZ = 0) {
	pos = Vector2f(pX, pY);
	z = pZ;
}

void oCircle::update() {
	s += 0.01 + 0.005 * (pos.x / 640)  + 0.005 * pos.y / 480;
}

void oCircle::refresh() {
	shape.setPosition(pos + Vector2f(sin(s)*20, sin(s*1.04 + pos.x * 10 * pos.y)*20));
	shape.setRadius(40);
	shape.setFillColor(Color(pos.y/pos.x*255,pos.y/pos.x*160+66,pos.y/pos.x*90, 255));
}

void oCircle::draw() {
	window.draw(shape);
}

void oSquare::refresh() {
}

void oSquare::draw() {
	boxi.setPosition(pos);
	boxi.setFillColor(Color(0,0,255,230));
	window.draw(boxi);
}
