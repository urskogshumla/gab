#ifndef __GABRIELLA_OBJECT_EXAMPLEOBJECTS__
#define __GABRIELLA_OBJECT_EXAMPLEOBJECTS__

#include "objects.hpp"
#include "SFML/Audio/SoundBuffer.hpp"
#include "SFML/Audio/Sound.hpp"
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

class audioHandler {
	private:
		SoundBuffer _snJump1;
		SoundBuffer _snJump2;
		SoundBuffer _snFinishLevel;
		SoundBuffer _snBelowScreen;
		SoundBuffer _snCheckPoint;
	public:
		int checkPoint;
		Sound snJump1;
		Sound snJump2;
		Sound snFinishLevel;
		Sound snBelowScreen;
		Sound snCheckPoint;
		audioHandler();
		~audioHandler() {};
};

extern audioHandler audio;

class oJumpController : public baseObject {
	public:
		oJumpController() {};
		~oJumpController() {};
};

class oJumpGround : public baseObject {
	public:
		oJumpGround();
		void draw();
		~oJumpGround() {};
};

class oJumpGoal : public baseObject {
	public:
		oJumpGoal();
		void draw();
		~oJumpGoal() {};
};

class oJumpCheckPoint : public baseObject {
	private:
		bool taken;
		bool alreadyTaken;
	public:
		oJumpCheckPoint();
		void draw();
		void update();
		~oJumpCheckPoint() {};
};

class oJumpGravity : public baseObject {
	public:
		bool taken;
		oJumpGravity();
		void draw();
		void update();
		~oJumpGravity();
};

class oJumpSquare : public baseObject {
	public:
		float cut(float, float, float);
		bool jumpAgain;
		Vector2f F;
		Vector2f v;
		oJumpSquare();
		void update();
		void draw();
		~oJumpSquare() {};
};








class oSquare : public baseObject {
	private:
		RectangleShape boxi;
	public:
		oSquare();
		void refresh();
		void draw();
		~oSquare() {};
		float z;
};

class oCircle : public baseObject {
	private:
		float s = 0;
		CircleShape shape;
	public:
		oCircle(float, float, float);
		oCircle() {};
		void update();
		void refresh();
		void draw();
		~oCircle() {};
};

#endif
