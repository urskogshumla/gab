#include "app.hpp"

#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include "window.hpp"
#include "input.hpp"
#include "view.hpp"
#include "../functions/console.hpp"

using namespace std;
using namespace sf;

baseApp::baseApp():
		fullscreen(false),
		size(VideoMode::getFullscreenModes()[3])
 {}

void baseApp::initWindow() {
	if (fullscreen)
		window.create(size, "Gabriella", Style::Fullscreen);
	else
		window.create(size, "Gabriella", Style::Titlebar);
	window.setFramerateLimit(50);
}

void baseApp::displayInfo() {
	debug.header("MESSAGE");
	debug.text("Welcome to Gabriella.");

	debug.header("PROGRAM STATUS");
	debug.var("VERSION", "DEV");
	debug.var("TYPE", "INDIEGAME");
	debug.var("DEVTYPE", "NONSERIOUS");
	debug.var("STATUS", "SLEEPY");
}

void baseApp::update() {
	input.update();
	view.update();
	view.refresh();
	view.draw();
	view.applyRoomChanges();
	window.display();
}

baseApp app;
