#ifndef __GABRIELLA_APP__
#define __GABRIELLA_APP__

#include <SFML/Window/VideoMode.hpp>

using namespace sf;
using namespace std;

class baseApp {
	private:
		bool fullscreen;
		VideoMode size;
	public:
		baseApp();

		void displayInfo();

		void initWindow();

		float k = 0;
		void update();
};

extern baseApp app;

#endif
