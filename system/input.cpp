#include "input.hpp"
#include "app.hpp"

using namespace sf;

void keyState::setLinkedKey(Keyboard::Key x) {
	link = x;
}
void keyState::update() {
	if (Keyboard::isKeyPressed(link)) {
		press = (!press) && passive;
		active = true;
		passive = false;
		release = false;
	}
	else {
		release = (!release) && active;
		passive = true;
		active = false;
		press = false;
	}
}
keyState::keyState(Keyboard::Key x) {
	setLinkedKey(x);
	press = false;
	release = false;
	active = false;
	passive = true;
}

baseInput::baseInput() {

}

//Update keyboard/joystick input
void baseInput::update() {
	UP.update();
	DOWN.update();
	LEFT.update();
	RIGHT.update();
	ALFA.update();
	BETA.update();
	ESCAPE.update();
}

baseInput input;
