#ifndef __GABRIELLA_INPUT__
#define __GABRIELLA_INPUT__

#include <SFML/Window/Keyboard.hpp>

using namespace sf;

class keyState {
	private:
		Keyboard::Key link;
	public:
		bool press;
		bool active;
		bool release;
		bool passive;

		keyState(Keyboard::Key);
		void update();
		void setLinkedKey(Keyboard::Key);
		~keyState() {};
};

class baseInput {
	public:
		keyState RIGHT = keyState(Keyboard::Right);
		keyState LEFT = keyState(Keyboard::Left);
		keyState UP = keyState(Keyboard::Up);
		keyState DOWN = keyState(Keyboard::Down);
		keyState ALFA = keyState(Keyboard::Z);
		keyState BETA = keyState(Keyboard::X);
		keyState ESCAPE = keyState(Keyboard::Escape);

		baseInput();
		void update();
		~baseInput() {};
};

extern baseInput input;

#endif
