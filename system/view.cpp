#define NEXTROOM_CAN_CHANGE true

#include "view.hpp"

#include <fstream>
#include <sstream>

#include "../objects/testObjects.hpp"
#include "../objects/kollisionsTest.hpp"

#include "../functions/getValuesFromString.hpp"
#include "../functions/console.hpp"

using namespace sf;
using namespace std;

objectPos::objectPos(int pId, float pX, float pY, float pZ) {
	id = pId;
	x = pX;
	y = pY;
	z = pZ;
}

objectPos::objectPos(vector<float> o) {
	//There should be a cooler syntax for this one.
	id = 0;
	x = 0;
	y = 0;
	z = 0;
	if (o.size()>3) z = o.at(3);
	if (o.size()>2) y = o.at(2);
	if (o.size()>1)	x = o.at(1);
	if (o.size()>0)	id = o.at(0);
}

room::room() {
	//Set objects boundaries?
}

room viewHandler::getCurrentRoom() {
	return rooms.at(currentRoom);
}

int viewHandler::getCurrentRoomIndex() {
	return currentRoom;
}

viewHandler::~viewHandler() {
	cleanView();
}

void room::addObject(objectPos a) {
	addObject(a.id, a.x, a.y, a.z);
}

void room::addObject(vector<float> o) {
	addObject(objectPos(o));
}

void room::addObject(int pId, float pX, float pY, float pZ = 0) {
	objectPos temp(pId, pX, pY, pZ);
	roomObjects.push_back(temp);
}

// Load rooms to rooms[]
void viewHandler::loadRooms() {
	fstream fHandle;
	string temp;
	vector<string> filenames;
	debug.header("Loading rooms");
	//Get all filenames objects/
	fHandle.open("rooms/index", ios_base::in);
	while (getline(fHandle, temp) != NULL) {
		filenames.push_back(temp);
	}
	fHandle.close();
	for (unsigned int i = 0; i < filenames.size(); i++) {
		//debug.text("Creating Room: " + filenames.at(i));
		room tempRoom;
		rooms.push_back(tempRoom);
		fHandle.open("rooms/" + filenames.at(i), ios_base::in | ios_base::out);
		while (getline(fHandle, temp) != NULL) {
			//debug.text("	Adding object: " + temp);
			if (temp == "") continue;
			rooms.at(i).addObject(getValuesFromString(temp));
		}
		fHandle.close();
	}
}

void viewHandler::cleanView() {
	baseObject *temp;
	for (unsigned int i = 0; i < objects.all.size(); i++) {
		temp = objects.all.at(i);
		delete temp;
	}
	//FIXME is there a function for erasing all content of a <vector>?
	while (objects.all.size() > 0) objects.all.pop_back();
	while (objects.solid.size() > 0) objects.solid.pop_back();
	while (objects.nonsolid.size() > 0) objects.nonsolid.pop_back();
	while (objects.active.size() > 0) objects.active.pop_back();
	while (objects.passive.size() > 0) objects.passive.pop_back();
	debug.header("Cleaning up..");
	//debug.text(std::to_string(objects.all.size()));
}

bool viewHandler::addObject(objectPos a) {
	return addObject(a, false);
}

bool viewHandler::addObject(objectPos a, bool addToRoom) {
	baseObject* temp;
	switch (a.id) {
		case 0:
			temp = new oCircle;
			debug.text("Created a circle!");
			break;
		case 1:
			temp = new oSquare;
			debug.header("MESSAGE");
			debug.text("You won! :)");
			break;
		case 2:
			temp = new oJumpController;
			debug.text("Created a controller for the jumping game");
			break;
		case 3:
			temp = new oJumpSquare;
			debug.text("Created a jumpSquare");
			objects.active.push_back(temp);
			break;
		case 4:
			temp = new oJumpGround;
			debug.text("Created some ground");
			objects.solid.push_back(temp);
			break;
		case 5:
			temp = new oJumpGoal;
			debug.text("Created a goal");
			objects.passive.push_back(temp);
			break;
		case 6:
			temp = new oJumpCheckPoint;
			debug.text("Created a checkpoint");
			break;
		case 7:
			temp = new oJumpGravity;
			debug.text("Created a gravity switch");
			break;
		case 8:
			temp = new oJumpGravity;
			debug.text("Created a gravity switch");
			temp->box.setFillColor(Color::Black);
			break;
		case 9:
			temp = new kolTest;
			debug.text("Created a kolTest!");
			break;
		default:
			return false;
	}
	temp->pos = Vector2f(a.x, a.y);
	temp->z = a.z;
	objects.all.push_back(temp);
	if (addToRoom)
		rooms.at(currentRoom).addObject(a);
	return true;
}

void viewHandler::applyRoomChanges() {
	if (nextRoom > (int)rooms.size() - 1 || nextRoom < 0) {
		nextRoom = -1;
		return;
	}
	cleanView();
	currentRoom = nextRoom;
	nextRoom = -1;
	debug.header("Switching room to " + to_string(currentRoom));
	for (unsigned int i = 0; i < rooms.at((unsigned int)currentRoom).roomObjects.size(); i++) {
		addObject(rooms.at((unsigned int)currentRoom).roomObjects.at(i));
	}
}

void viewHandler::restartRoom() {
	nextRoom = currentRoom;
}

bool viewHandler::setRoom(int pR) {
	if (pR > (int)rooms.size() - 1 || pR < 0)	return false;
	if (nextRoom == -1 || NEXTROOM_CAN_CHANGE) {
		nextRoom = pR;
		return true;
	}
	return false;
}

bool viewHandler::changeRoom(int i) {
	return setRoom(currentRoom + i);
}

//Update game time, sounds, active objects and viewport
void viewHandler::update() {
	//UPDATE TIME
	for (unsigned int i = 0; i < objects.all.size(); i++) {
		objects.all.at(i)->update();
	}
}

//Refresh animations and graphic positions in the view (appr. 20 ups?)
void viewHandler::refresh() {
	for (unsigned int i = 0; i < objects.all.size(); i++) {
		objects.all.at(i)->refresh();
	}
}

//Draw all stuff in the viewport (appr. 60 fps or 40 fps?)
void viewHandler::draw() {
	//REORDER BY Z?
	for (unsigned int i = 0; i < objects.all.size(); i++) {
		objects.all.at(i)->draw();
	}
}

viewHandler view;
