#ifndef __GABRIELLA_VIEW__
#define __GABRIELLA_VIEW__

#include <vector>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/View.hpp>
#include "../objects/objects.hpp" //objects.hpp that links to the different base types?

using namespace std;

class objectPos {
	public:
		float x, y, z;
		int id;

		objectPos(vector<float>);
		objectPos(int, float, float, float);
		~objectPos() {};
};

class room {
	public:
		room();
		vector<objectPos> roomObjects;
		Vector2f size, viewPosition, viewSize;
		void addObject(vector<float>);
		void addObject(objectPos);
		void addObject(int, float, float, float); //Custom Startcode?
		~room() {};
};

class viewHandler {
	private:
		vector<room> rooms;
		int currentRoom = 0;
		int nextRoom = -1;
		View windowView;

	public:
		Vector2f viewPosition;
		Vector2f viewSize;

		viewHandler() {};

		void cleanView();
		void loadRooms();
		bool addObject(objectPos);
		bool addObject(objectPos, bool);
		bool setRoom(int);
		bool setRoom(unsigned int);
		bool changeRoom(int);
		void restartRoom();
		void applyRoomChanges();
		int getCurrentRoomIndex();
		room getCurrentRoom();

		void update();
		void refresh();
		void draw();

		~viewHandler();
};

extern viewHandler view;

#endif
